<?php

include_once("../conf.php");

$result = mysqli_query($mysqli, "SELECT airports.id, airports.name, airports.location, countries.name as country 
FROM airports 
LEFT JOIN countries ON airports.country=countries.id
ORDER BY id DESC");
$result_airlines = mysqli_query($mysqli, "SELECT airlines.*, airports.id as airport
FROM airlines 
LEFT JOIN airlines_airports ON airlines_airports.airline_id=airlines.id
LEFT JOIN airports ON airlines_airports.airport_id=airports.id");

$airlines=[];
while($air = mysqli_fetch_array($result_airlines)) {
	$airlines[]=$air;
}
?>

<html>
<head>
    <title>Homepage</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
    <?php
    include_once("../layouts/navbar.html");
    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="mt-5 mb-3 clearfix">
                    <h2 class="pull-left">Airports Details</h2>
                    <a href="add.php" class="btn btn-success pull-right mr-4"><i class="fa fa-plus"></i> Add New Airport</a>
                </div>

                <table width='80%' class="table table-bordered table-striped">

                <tr>
                    <th>Name</th> <th>Country</th> <th>Location</th><th>Airlines</th><th>Actions</th>
                </tr>
                <?php
                while($airport_data = mysqli_fetch_array($result)) {
                    echo "<tr>";
                    echo "<td>".$airport_data['name']."</td>";
                    echo "<td>".$airport_data['country']."</td>";
                    echo "<td>".$airport_data['location']."</td>";
                    echo "<td>";
                    foreach ($airlines as $airline){
                        if($airport_data['id'] === $airline['airport']){
                            echo $airline['name']."<br>";
                        }
                    } 
                    echo "</td>";
                    echo '<td><a href="read.php?id='. $airport_data['id'] .'" class="mr-3" title="View Record" data-toggle="tooltip"><span class="fa fa-eye text-info"></span></a><a href="edit.php?id='.$airport_data['id'].'" class="mr-3" title="Update Record" data-toggle="tooltip"><span class="fa fa-pencil"></span></a> <a href="delete.php?id='. $airport_data['id'].'" title="Delete Record" data-toggle="tooltip"><span class="fa fa-trash text-danger"></span></a></td></tr>';
                }
                ?>
                </table>
            </div>
        </div>
    </div>
</body>
</html>