<?php

include_once("../conf.php");
$id = $_GET['id'];

$result_countries = mysqli_query($mysqli, "SELECT * FROM countries ORDER BY id DESC");
$result_airlines = mysqli_query($mysqli, "SELECT * FROM airlines ORDER BY id DESC");

$sql = "SELECT * FROM airports WHERE id=?";    
if($stmt = mysqli_prepare($mysqli, $sql)){
	mysqli_stmt_bind_param($stmt, "i", $param_id);
	$param_id = trim($id);
	if(mysqli_stmt_execute($stmt)){
		$result = mysqli_stmt_get_result($stmt);            
	} else{
		echo "Oops! Something went wrong. Please try again later.";
	}
}

$sql = "SELECT airline_id FROM airlines_airports WHERE airport_id=?";    
if($stmt = mysqli_prepare($mysqli, $sql)){
	mysqli_stmt_bind_param($stmt, "i", $param_id);
	$param_id = trim($id);
	if(mysqli_stmt_execute($stmt)){
		$result_air = mysqli_stmt_get_result($stmt);            
	} else{
		echo "Oops! Something went wrong. Please try again later.";
	}
}
mysqli_stmt_close($stmt);

$selected_airlines=[];
while($air = mysqli_fetch_array($result_air)) {
	$selected_airlines[]=$air['airline_id'];
}

$airport_data = mysqli_fetch_assoc($result);
$name = $airport_data['name'];
$country = $airport_data['country'];
$location = $airport_data['location'];
?>
<html>
<head>
	<title>Airport Data</title>
	<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <link rel="stylesheet" type="text/css" href="./style.css" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="./source.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
<?php
    include_once("../layouts/navbar.html");
?>
<h3 class="m-4">Airport</h3>
<div class="m-4 p-4 w-75">
	<form name="read">
			<div class="form-group">
				<label for="name" class="font-weight-bold">Name</label>
				<input type="text" class="form-control-plaintext" name="name" value=<?php echo $name;?> readonly>
			</div>
			<div class="form-group">
			<label for="country" class="font-weight-bold">Country</label>
					<?php
					while($countries = mysqli_fetch_array($result_countries)) {						 
					   if($countries['id'] === $country){
						   echo '<input type="text" class="form-control-plaintext" name="name" value="'.$countries['name'].'" readonly>';;
					   }
						
				   }
					?>
            
			</div>
			<div class="form-group">
			<label for="location" class="font-weight-bold">Location</label>
				<td><input id="location" type="text" name="location" class="form-control-plaintext" value="<?php echo $location;?>" readonly></td>
			</div>
			<div class="form-group">
			<label for="airlines" class="font-weight-bold">Airlines</label>				
                <?php
                while($airlines = mysqli_fetch_array($result_airlines)) {
                    if (isset($selected_airlines) && in_array($airlines['id'], $selected_airlines)) {
                        echo '<input type="text" class="form-control-plaintext" name="name" value="'.$airlines['name'].'" readonly>';
                    }
                }
                ?>
			</div>
	</form>
</div>	
</body>
</html>
