<?php

include_once("../conf.php");
include_once("../validation/validation.php");


$result = mysqli_query($mysqli, "SELECT * FROM countries ORDER BY id DESC");
$result_airlines = mysqli_query($mysqli, "SELECT * FROM airlines ORDER BY id DESC");

$success_message = "";

if(isset($_POST['Submit'])) {
	$name = $_POST['name'];
	$name_err = validate_name($name);
	
	$country = $_POST['country'];
	$country_err = validate_country($country);

	$location = $_POST['location'];
	$location_err = validate_location($location);
	$airlines = $_POST['airlines'];
	$airlines_err = validate_airlines($airlines);
	if ($name_err === null && $country_err === null && $location_err === null && $airlines_err === null) {
		$sql = "INSERT INTO airports (name, country, location) VALUES (?, ?, ?)";
		if($stmt = mysqli_prepare($mysqli, $sql)){				
			
			mysqli_stmt_bind_param($stmt, "sss", $param_name, $param_country, $param_location);				
			
			$param_name = $name;
			$param_country = $country;
			$param_location = $location;				
			
			if(mysqli_stmt_execute($stmt)){
				$airport_id = $mysqli->insert_id;
				foreach ($airlines as $airline) {
					$sql = "INSERT INTO airlines_airports (airline_id, airport_id) VALUES (?, ?)";
					if($stmt = mysqli_prepare($mysqli, $sql)){
						mysqli_stmt_bind_param($stmt, "ii", $param_airline, $param_airport);
						$param_airline = $airline;
						$param_airport = $airport_id;
						if(!mysqli_stmt_execute($stmt)){
							echo "Oops! Something went wrong. Please try again later.";
						}
					}else{
						echo "Oops! Something went wrong. Please try again later.";
					}
					
				}
			} else{
				echo "Oops! Something went wrong. Please try again later.";
			}
			mysqli_stmt_close($stmt);
		}				
		
		$success_message = '<div class="alert alert-success" role="alert">Airport added successfully. <a href="index.php">View Airports</a> </div>';
	}
}
?>	
<html>
<head>
	<title>Add Airports</title>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="../js/source.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
	<?php
    include_once("../layouts/navbar.html");
    if($success_message !== ""){
		echo $success_message;
	}
	?>
	<h3 class="m-4">New Airport</h3>
<div class="m-4 p-4 w-75">
	<form action="add.php" method="post" name="create">
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" class="form-control <?php echo (!empty($name_err)) ? 'is-invalid' : ''; ?>" name="name" required>
				<span class="invalid-feedback"><?php echo $name_err;?></span>
			</div>
			<div class="form-group">
			<label for="country">Country</label>
			
				<select name="country" id="country" class="form-control <?php echo (!empty($country_err)) ? 'is-invalid' : ''; ?>" required>
					<?php
					while($countries = mysqli_fetch_array($result)) {
						echo "<option value=".$countries['id'].">".$countries['name']."</option>";
					}
					?>
				</select>
				<span class="invalid-feedback"><?php echo $country_err;?></span>
			</div>
			<div class="form-group">
			<label for="location">Location</label>
				<td><input id="location" type="text" name="location" class="form-control <?php echo (!empty($location_err)) ? 'is-invalid' : ''; ?>" required></td>
				<span class="invalid-feedback"><?php echo $location_err;?></span>
			</div>
			<div id="map"></div>
			<div class="form-group">
				<label for="airlines">Airlines</label>				
				<select name="airlines[]" id="airline" multiple class="form-control <?php echo (!empty($airlines_err)) ? 'is-invalid' : ''; ?>" required>
					<?php
					while($airlines = mysqli_fetch_array($result_airlines)) {
						echo "<option value=".$airlines['id'].">".$airlines['name']."</option>";
					}
					?>
				</select>
				<span class="invalid-feedback"><?php echo $airlines_err;?></span>
			</div>
			<div class="form-group">
				<input type="submit" name="Submit" class="btn btn-success" value="Add">
			</div>
	</form>
</div>

<script
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3SM1Mf7OSj9pbEzlFko6rz991o8tR1Fc&callback=initMap&v=weekly"
  async
></script>
</body>
</html>