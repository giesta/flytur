<?php

include_once("../conf.php");
include_once("../validation/validation.php");

if(isset($_POST['update']))
{
	$id = $_POST['id'];

	$name = $_POST['name'];
	$name_err = validate_name($name);
	
	$country = $_POST['country'];
	$country_err = validate_country($country);

	$location = $_POST['location'];
	$location_err = validate_location($location);

	$airlines = $_POST['airlines'];
	$airlines_err = validate_airlines($airlines);

	if ($name_err === null && $country_err === null && $location_err === null && $airlines_err === null) {
		
		$sql = "UPDATE airports SET name=?,country=?,location=? WHERE id=?";
		
		if($stmt = mysqli_prepare($mysqli, $sql)){
			mysqli_stmt_bind_param($stmt, "sisi", $param_name, $param_country, $param_location, $param_id);
			
			$param_id = trim($id);
			$param_name=$name;
			$param_country=$country;
			$param_location=$location;
			if(!mysqli_stmt_execute($stmt)){
				echo "Oops! Something went wrong. Please try again later.";
			}
		}
		$sql = "DELETE FROM airlines_airports WHERE airport_id=?";
		
		if($stmt = mysqli_prepare($mysqli, $sql)){
			mysqli_stmt_bind_param($stmt, "i", $param_id);
			if(!mysqli_stmt_execute($stmt)){
				echo "Oops! Something went wrong. Please try again later.";
			}
		}
		
		foreach ($airlines as $airline) {
			$sql = "INSERT INTO airlines_airports (airline_id, airport_id) VALUES (?, ?)";
			if($stmt = mysqli_prepare($mysqli, $sql)){
				mysqli_stmt_bind_param($stmt, "ii", $param_airline, $param_airport);
				$param_airline = $airline;
				$param_airport = $id;
				if(!mysqli_stmt_execute($stmt)){
					echo "Oops! Something went wrong. Please try again later.";
				}
			}else{
				echo "Oops! Something went wrong. Please try again later.";
			}
		}
		mysqli_stmt_close($stmt);
		header("Location: index.php");
	}
}
?>
<?php
if(isset($_GET['id'])){
	$id = $_GET['id'];
}

$result_countries = mysqli_query($mysqli, "SELECT * FROM countries ORDER BY id DESC");
$result_airlines = mysqli_query($mysqli, "SELECT * FROM airlines ORDER BY id DESC");

$sql = "SELECT * FROM airports WHERE id=?";    
if($stmt = mysqli_prepare($mysqli, $sql)){
	mysqli_stmt_bind_param($stmt, "i", $param_id);
	$param_id = trim($id);
	if(mysqli_stmt_execute($stmt)){
		$result = mysqli_stmt_get_result($stmt);            
	} else{
		echo "Oops! Something went wrong. Please try again later.";
	}
}

$sql = "SELECT airline_id FROM airlines_airports WHERE airport_id=?";    
if($stmt = mysqli_prepare($mysqli, $sql)){
	mysqli_stmt_bind_param($stmt, "i", $param_id);
	$param_id = trim($id);
	if(mysqli_stmt_execute($stmt)){
		$result_air = mysqli_stmt_get_result($stmt);            
	} else{
		echo "Oops! Something went wrong. Please try again later.";
	}
}


$selected_airlines=[];
while($air = mysqli_fetch_array($result_air)) {
	$selected_airlines[]=$air['airline_id'];
}

$airport_data = mysqli_fetch_assoc($result);
if(isset($airport_data['name'])){
	$name = $airport_data['name'];
}
if(isset($airport_data['country'])){
	$country = $airport_data['country'];
}
if(isset($airport_data['country'])){
	$location = $airport_data['location'];
}

mysqli_stmt_close($stmt);
?>
<html>
<head>
	<title>Edit Airport Data</title>
	<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="../js/source.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
<?php
    include_once("../layouts/navbar.html");
?>
<h3 class="m-4">Update Airport</h3>
<div class="m-4 p-4 w-75">
	<form action="edit.php" method="post" name="edit">
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" class="form-control <?php echo (!empty($name_err)) ? 'is-invalid' : ''; ?>" name="name" value=<?php echo $name;?> required>
				<span class="invalid-feedback"><?php echo $name_err;?></span>
			</div>
			<div class="form-group">
				<label for="country">Country</label>			
				<select name="country" id="country" class="form-control <?php echo (!empty($country_err)) ? 'is-invalid' : ''; ?>" required>
					<?php
					while($countries = mysqli_fetch_array($result_countries)) {
						echo '<option value="'.$countries['id'].'"';						 
					   if($countries['id'] === $country){
						   echo "selected";
					   }
						echo ">".$countries['name']."</option>";
				   }
					?>
				</select>
				<span class="invalid-feedback"><?php echo $country_err;?></span>
            
			</div>
			<div class="form-group">
			<label for="location">Location</label>
				<td><input id="location" type="text" name="location" class="form-control <?php echo (!empty($location_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $location;?>" required></td>
				<span class="invalid-feedback"><?php echo $location_err;?></span>
			</div>
			<div id="map"></div>
			<div class="form-group">
				<label for="airlines">Airlines</label>				
				<select name="airlines[]" id="airline" multiple class="form-control <?php echo (!empty($airlines_err)) ? 'is-invalid' : ''; ?>" required>
					<?php
					while($airlines = mysqli_fetch_array($result_airlines)) {
						echo '<option value="'.$airlines['id'].'"';
						if (isset($selected_airlines) && in_array($airlines['id'], $selected_airlines)) {
						   echo "selected";
						}						
						echo ">".$airlines['name']."</option>";
					   }
					?>
				</select>
				<span class="invalid-feedback"><?php echo $airlines_err;?></span>
			</div>
			<div class="form-group">
				<input type="hidden" name="id" value=<?php echo $_GET['id'];?>>
				<input type="submit" name="update" class="btn btn-success" value="Update">
			</div>
	</form>
</div>	
	
<script
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3SM1Mf7OSj9pbEzlFko6rz991o8tR1Fc&callback=initMap&v=weekly"
  async
></script>
</body>
</html>
