<?php
include_once("../conf.php");
include_once("../validation/validation.php");


$result = mysqli_query($mysqli, "SELECT * FROM countries ORDER BY id DESC");
$success_message = "";
if(isset($_POST['Submit'])) {
	$name = $_POST['name'];
	$name_err = validate_name($name);

	$country = $_POST['country'];
	$country_err = validate_country($country);

	if ($name_err === null && $country_err === null) {
		$sql = "INSERT INTO airlines(name,country) VALUES (?, ?)";
		if($stmt = mysqli_prepare($mysqli, $sql)){				
			
			mysqli_stmt_bind_param($stmt, "ss", $param_name, $param_country);				
			
			$param_name = $name;
			$param_country = $country;				
			
			if(!mysqli_stmt_execute($stmt)){
				echo "Oops! Something went wrong. Please try again later.";
			}
		}				
		mysqli_stmt_close($stmt);
		
		$success_message = '<div class="alert alert-success" role="alert">Airline added successfully. <a href="index.php">View Airlines</a> </div>';
	}
}
?>
<html>
<head>
	<title>Add Airlines</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
<?php
    include_once("../layouts/navbar.html");	
	if($success_message !== ""){
		echo $success_message;
	}	
?>
<h3 class="m-4">New Airline</h3>
<div class="m-4 p-4 w-50">
	<form action="add.php" method="post" name="create">
		<div class="form-group">
			<label for="name">Name</label>
			<input type="text" class="form-control <?php echo (!empty($name_err)) ? 'is-invalid' : ''; ?>" name="name" required>
			<span class="invalid-feedback"><?php echo $name_err;?></span>
		</div>
		<div class="form-group">
			<label for="name">Country</label>
			<select name="country" id="country" class="form-control <?php echo (!empty($country_err)) ? 'is-invalid' : ''; ?>" required>
				<?php
				while($countries = mysqli_fetch_array($result)) {
					echo "<option value=".$countries['id'].">".$countries['name']."</option>";
				}
				?>
			</select>
			<span class="invalid-feedback"><?php echo $country_err;?></span>
		</div>
		<div class="form-group">
			<input type="submit" name="Submit" class="btn btn-success" value="Add">
		</div>
	</form>
</div>
</body>
</html>