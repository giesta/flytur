<?php
include_once("../conf.php");
include_once("../validation/validation.php");

if(isset($_POST['update']))
{
	$id = $_POST['id'];

	$name = $_POST['name'];
	$name_err = validate_name($name);

	$country = $_POST['country'];
	$country_err = validate_country($country);
	if ($name_err === null && $country_err === null) {
		$sql = "UPDATE airlines SET name=?,country=? WHERE id=?";
		if($stmt = mysqli_prepare($mysqli, $sql)){				
			
			mysqli_stmt_bind_param($stmt, "ssi", $param_name, $param_country, $param_id);				
			
			$param_id = trim($id);
			$param_name = $name;
			$param_country = $country;				
			
			if(!mysqli_stmt_execute($stmt)){
				echo "Oops! Something went wrong. Please try again later.";
			}
		}				
		mysqli_stmt_close($stmt);
	}

	header("Location: index.php");
}
?>
<?php
$id = $_GET['id'];

$result_countries = mysqli_query($mysqli, "SELECT * FROM countries ORDER BY id DESC");

$sql = "SELECT * FROM airlines WHERE id=?";    
if($stmt = mysqli_prepare($mysqli, $sql)){
	mysqli_stmt_bind_param($stmt, "i", $param_id);
	$param_id = trim($id);
	if(mysqli_stmt_execute($stmt)){
		$result = mysqli_stmt_get_result($stmt);            
	} else{
		echo "Oops! Something went wrong. Please try again later.";
	}
	mysqli_stmt_close($stmt);
}

while($airlines_data = mysqli_fetch_array($result))
{
	$name = $airlines_data['name'];
	$country = $airlines_data['country'];
}
?>
<html>
<head>
	<title>Edit Airline Data</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
<?php
    include_once("../layouts/navbar.html");
?>
<h3 class="m-4">New Airline</h3>
<div class="m-4 p-4 w-50">
	<form action="edit.php" method="post" name="update">
		<div class="form-group">
			<label for="name">Name</label>
			<input type="text" class="form-control" name="name" value=<?php echo $name;?> required>
		</div>
		<div class="form-group">
			<label for="name">Country</label>
			<select name="country" id="country" class="form-control" required>
				<?php
				while($countries = mysqli_fetch_array($result_countries)) {
					echo '<option value="'.$countries['id'].'"';						 
				   if($countries['id'] === $country){
					   echo "selected";
				   }
					echo ">".$countries['name']."</option>";
			   }
				?>
			</select>
		</div>
		<div class="form-group">
			<input type="hidden" name="id" value=<?php echo $_GET['id'];?>>
			<input type="submit" name="update" class="btn btn-success" value="Update">
		</div>
	</form>
</div>
</body>
</html>