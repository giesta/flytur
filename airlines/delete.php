<?php
include_once("../conf.php");

$id = $_GET['id'];

$sql = "DELETE FROM airlines WHERE id=?";    
if($stmt = mysqli_prepare($mysqli, $sql)){
    mysqli_stmt_bind_param($stmt, "i", $param_id);
    $param_id = trim($id);
    if(!mysqli_stmt_execute($stmt)){
        echo "Oops! Something went wrong. Please try again later.";
    }
}
header("Location:index.php");
?>