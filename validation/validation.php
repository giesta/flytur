<?php

function validate_name($name){
    if(empty($name)){
         return "Please enter a name.";
    } elseif(!filter_var($name, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^(?!\d)[a-zA-z\d ]+$/")))){
        return "Please enter a valid name.";
    }
    return null;
}

function validate_country($country){
    if(empty($country)){
         return "Please select a country.";
    } elseif($country < 1){
        return "Please select a country.";
    }
    return null;
}

function validate_location($location){
    if(empty($location)){
         return "Please select a location.";
    } elseif(!preg_match('/^\([-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?), [-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)\)$/', $location)){
        return "Please enter a valid location.";
    }
    return null;
}   

function validate_airlines($airlines){
    if(empty($airlines)){
         return "Please select an airline.";
    } elseif(count($airlines) < 1){
        return "Please select an airline.";
    }
    return null;
}
    
?>